class EditableList extends HTMLElement {
    #root;

    constructor() {
        super();
        this.#root = this.attachShadow({ mode: 'closed' })
        /*Vykresleni html komponenty*/
        this.#render();
        /*Udalost tlacitka pro přidavani*/
        const ins = this.#root.querySelector('.insert');
        const btn = this.#root.querySelector('.btn');
        btn.onclick = (ev) => {
            this.#addNew(ins.value)
            ins.value = '';
        }
    }

    //set h1 title text after insert into DOM
    connectedCallback() {
        const h = this.#root.querySelector('h1.title');
        h.textContent = this.getAttribute('title');
    }

    //render base html into shadow DOM
    #render() {
        const tmpl = `
        <link rel="stylesheet" href="todo.css">
        <h1 class="title"></h1>
        <input type="text" class="insert" placeholder="Name of next TODO...">
        <input type="button" value="Vložit" class="btn">
        <ul></ul>
        `
        this.#root.innerHTML = tmpl;
    }

    // add new task using inserting new <li> element and
    // bind click events
    #addNew(task) {
        if (task.length !== 0) {
            const ul = this.#root.querySelector('ul');
            const li = document.createElement('li');
            li.title = "Check/Uncheck";
            ul.appendChild(li);
            li.textContent = task;
            li.innerHTML += `<span title="Delete" class="cross">x</span>`;
            li.onclick = this.#handleClicks;
        } else {
            alert('Název úkolu nem§že být prázdný')
        }
    }

    //onclick handler for li and li > span
    #handleClicks(ev) {
        const el = ev.target;
        if (el.tagName == 'LI') {
            el.classList.toggle("checked")
        } else if (el.tagName == 'SPAN' && el.classList.contains('cross')) {
            if (el.parentElement.classList.contains('checked')) {
                if (window.confirm(`Opravdu chcete smazat úkol: "${el.parentElement.textContent}"`)) {
                    el.parentElement.parentElement.removeChild(el.parentElement);
                }
            } else {
                alert('Nemůžete smazat úkol, který není označený za splněný!')
            }
        }
    }
}

customElements.define('todo-list', EditableList);
