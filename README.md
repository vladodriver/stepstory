## Using TODO list js WebComponent

1. load todo.js file using ex: `<script src="todo.js" defer></script>`

2. insert `<todo-list title="YourTitle"></todo-list>` into your HTML code
3. or use js & DOM for add element at runtime: \
`const td = document.createElement('todo-list')`\
`td.setAttribute('title', 'YourTitle')`\
`document.body.appendChild(td)`

